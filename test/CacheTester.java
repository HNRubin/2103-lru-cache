import org.junit.Test;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Code to test an <tt>LRUCache</tt> implementation.
 */
public class CacheTester {
    @Test
    public void leastRecentlyUsedIsCorrect() {
        DataProvider<Integer, Integer> provider = new DirectIntProvider();
        Cache<Integer, Integer> cache = new LRUCache<>(provider, 5);

        //Populate the cache
        assertEquals((int) cache.get(0), 0);
        assertEquals((int) cache.get(1), 1);
        assertEquals((int) cache.get(2), 2);
        assertEquals((int) cache.get(3), 3);
        assertEquals((int) cache.get(4), 4);
        assertEquals(cache.getNumMisses(), 5);

        cache.get(5); //Get something outside capacity
        assertEquals(cache.getNumMisses(), 6); //Verify Miss
        //Verify 0 was removed from the cache.
        cache.get(0);
        assertEquals(cache.getNumMisses(), 7);

        //Verify 1 was removed from the cache.
        cache.get(1);
        assertEquals(cache.getNumMisses(), 8);
    }

    @Test
    public void testCacheIsUsed() {
        DirectIntProvider provider = new DirectIntProvider();
        Cache<Integer, Integer> cache = new LRUCache<>(provider, 5);

        //Populate the cache
        assertEquals((int) cache.get(0), 0);
        assertEquals((int) cache.get(1), 1);
        assertEquals((int) cache.get(2), 2);
        assertEquals((int) cache.get(3), 3);
        assertEquals((int) cache.get(4), 4);
        assertEquals(cache.getNumMisses(), 5);

        for (int i = 0; i < 1000; i++) {
            cache.get(i % 5);
        }

        assertEquals(cache.getNumMisses(), 5);
        assertEquals(provider.getRequestHistory().size(), 5);
    }


    @Test
    public void testCapacity() {
        DirectIntProvider provider = new DirectIntProvider();
        Cache<Integer, Integer> cache = new LRUCache<>(provider, 10);
        for (int i = 0; i < 100; i++) {
            cache.get(i % 10);
        }
        assertEquals(cache.getNumMisses(), 10);
        assertEquals(provider.getRequestHistory().size(), 10);
        cache = new LRUCache<>(provider, 5);
        for (int i = 0; i < 100; i++) {
            cache.get(i % 10);
        }
        assertEquals(cache.getNumMisses(), 100);
        assertEquals(provider.getRequestHistory().size(), 110);
    }

    @Test
    public void testNull() {
        DataProvider provider = new NullProvider<>();
        Cache cache = new LRUCache<>(provider, 10);
        assertNull(cache.get(5));
        assertEquals(cache.getNumMisses(), 1);
        assertNull(cache.get(5));
        assertEquals(cache.getNumMisses(), 1);
        assertNull(cache.get(null));
        assertEquals(cache.getNumMisses(), 2);
        assertNull(cache.get(null));
        assertEquals(cache.getNumMisses(), 2);
    }

    @Test
    public void testExampleLRU() {
        DirectStringProvider provider = new DirectStringProvider();
        LRUCache<String, String> cache = new LRUCache<>(provider, 2);
        assertEquals(cache.getNumMisses(), 0);

        assertEquals(cache.get("smurf"), "smurf");
        assertEquals(provider.getRequestHistory(), Arrays.asList("smurf"));
        assertEquals(cache.getNumMisses(), 1);

        assertEquals(cache.get("garfield"), "garfield");
        assertEquals(provider.getRequestHistory(), Arrays.asList("smurf", "garfield"));
        assertEquals(cache.getNumMisses(), 2);

        assertEquals(cache.get("marge"), "marge");
        assertEquals(provider.getRequestHistory(), Arrays.asList("smurf", "garfield", "marge"));
        assertEquals(cache.getNumMisses(), 3);

        assertEquals(cache.get("snoopy"), "snoopy");
        assertEquals(provider.getRequestHistory(), Arrays.asList("smurf", "garfield", "marge", "snoopy"));
        assertEquals(cache.getNumMisses(), 4);

        assertEquals(cache.get("garfield"), "garfield");
        assertEquals(provider.getRequestHistory(), Arrays.asList("smurf", "garfield", "marge", "snoopy", "garfield"));
        assertEquals(cache.getNumMisses(), 5);

        assertEquals(cache.get("snoopy"), "snoopy");
        assertEquals(provider.getRequestHistory(), Arrays.asList("smurf", "garfield", "marge", "snoopy", "garfield"));
        assertEquals(cache.getNumMisses(), 5);
    }

    @Test
    public void testRepeatedCalls() {
        DirectIntProvider provider = new DirectIntProvider();
        LRUCache<Integer, Integer> cache = new LRUCache<>(provider, 2);
        assertEquals(cache.getNumMisses(), 0);
        for (int i = 0; i < 5000; i++) {
            assertEquals((int) cache.get(i), i);
        }
        assertEquals(cache.getNumMisses(), 5000);
        //reset
        cache = new LRUCache<>(provider, 2);
        for (int i = 0; i < 5000; i++) {
            assertEquals((int) cache.get(0), 0);
        }
        assertEquals(cache.getNumMisses(), 1);
    }

    @Test
    public void testWeirdCapacities() {
        DirectIntProvider provider = new DirectIntProvider();
        LRUCache<Integer, Integer> cache = new LRUCache<>(provider, 0);
        assertEquals(cache.getNumMisses(), 0);
        for (int i = 0; i < 10; i++) {
            cache.get(1);
            cache.get(2);
        }
        assertEquals(cache.getNumMisses(), 20);
        assertEquals(provider.requestHistory.size(), 20);


        DirectIntProvider provider2 = new DirectIntProvider();
        LRUCache<Integer, Integer> cache2 = new LRUCache<>(provider2, 1);
        assertEquals(cache2.getNumMisses(), 0);
        for (int i = 0; i < 10; i++) {
            cache2.get(1);
            cache2.get(2);
        }
        assertEquals(cache2.getNumMisses(), 20);
        assertEquals(provider2.requestHistory.size(), 20);

        DirectIntProvider provider3 = new DirectIntProvider();
        LRUCache<Integer, Integer> cache3 = new LRUCache<>(provider3, 2);
        assertEquals(cache3.getNumMisses(), 0);
        for (int i = 0; i < 10; i++) {
            cache3.get(1);
            cache3.get(2);
        }
        assertEquals(cache3.getNumMisses(), 2);
        assertEquals(provider3.requestHistory.size(), 2);
    }


    @Test
    public void testIfConstantTime() {
        DirectIntProvider provider = new DirectIntProvider();
        int trials = 4000;
        double[] timeCost = new double[trials];
        for (int i = 0; i < trials; i++) {
            LRUCache<Integer, Integer> cache = new LRUCache<>(provider, 1000 * i);
            long overallTime = 0;
            for (int j = 0; j < trials; j++) {
                long now = System.nanoTime();
                for (int k = 0; k < 250; k++) {
                    cache.get(k);
                }
                long diff = System.nanoTime() - now;
                overallTime += diff;
            }
            timeCost[i] = ((double) overallTime / (double) trials);
        }
        int numGreater = 0;
        int numToDivide = 0;
        for (int i = 0; i < trials - 1; i++) {
            for (int j = i + 1; j < trials; j++) {
                if (timeCost[i] <= timeCost[j]) {
                    numGreater++;
                }
                numToDivide++;
            }
        }
        double avg = (double) numGreater / (double) numToDivide;
        //System.out.println(avg);
        assertTrue(
                0.35 <= avg && avg <= 0.65
        );
    }

    @Test
    public void testIfConstantTime2() {
        DirectIntProvider provider = new DirectIntProvider();
        int trials = 500;
        double[] timeCost = new double[trials];
        for (int i = 0; i < trials; i++) {
            long overallTime = 0;
            for (int j = 0; j < trials; j++) {
                LRUCache<Integer, Integer> cache = new LRUCache<>(provider, 1000 * i);
                long now = System.nanoTime();
                for (int k = 0; k < 50; k++) {
                    cache.get(k);
                }
                long diff = System.nanoTime() - now;
                overallTime += diff;
            }
            timeCost[i] = ((double) overallTime / (double) trials);
        }
        int numGreater = 0;
        int numToDivide = 0;
        for (int i = 0; i < trials - 1; i++) {
            for (int j = i + 1; j < trials; j++) {
                if (timeCost[i] <= timeCost[j]) {
                    numGreater++;
                }
                numToDivide++;
            }
        }
        double avg = (double) numGreater / (double) numToDivide;
        System.out.println(avg);
        assertTrue(
                0.35 <= avg && avg <= 0.65
        );
    }

    @Test
    public void testConstantTimeAlgorithm() {
        BadDataProvider provider = new BadDataProvider();
        int trials = 50;
        double[] timeCost = new double[trials];
        for (int i = 0; i < trials; i++) {
            LRUCache<Integer, Integer> cache = new LRUCache<>(provider, 1000 * i);
            long overallTime = 0;
            for (int j = 0; j < trials; j++) {
                long now = System.nanoTime();
                cache.get(i);
                long diff = System.nanoTime() - now;
                overallTime += diff;
            }
            timeCost[i] = ((double) overallTime / (double) trials);
        }
        int numGreater = 0;
        int numToDivide = 0;
        for (int i = 0; i < trials - 1; i++) {
            for (int j = i + 1; j < trials; j++) {
                if (timeCost[i] <= timeCost[j]) {
                    numGreater++;
                }
                numToDivide++;
            }
        }
        double avg = (double) numGreater / (double) numToDivide;
        assertTrue(avg > .6);
    }

    @Test
    public void testKeepsTrackOfUsed() {
        DirectStringProvider provider = new DirectStringProvider();
        LRUCache<String, String> cache = new LRUCache<>(provider, 3);
        cache.get("bob");
        cache.get("pat");
        cache.get("cat");
        assertEquals(cache.getNumMisses(), 3);
        cache.get("bob");
        assertEquals(cache.getNumMisses(), 3);
        cache.get("whatever");
        assertEquals(cache.getNumMisses(), 4);
        cache.get("bob");
        assertEquals(cache.getNumMisses(), 4);
    }


    public static class DirectStringProvider implements DataProvider<String, String> {

        private final List<String> requestHistory = new ArrayList<>();

        @Override
        public String get(String key) {
            requestHistory.add(key);
            return key;
        }

        public List<String> getRequestHistory() {
            return requestHistory;
        }
    }

    public static class DirectIntProvider implements DataProvider<Integer, Integer> {

        private final List<Integer> requestHistory = new ArrayList<>();


        @Override
        public Integer get(Integer key) {
            requestHistory.add(key);
            return key;
        }

        public List<Integer> getRequestHistory() {
            return requestHistory;
        }

    }

    public static class NullProvider<U> implements DataProvider<U, U> {
        @Override
        public U get(U key) {
            return null;
        }
    }

    public static class BadDataProvider implements DataProvider<Integer, Integer> {
        @Override
        public Integer get(Integer key) {
            try {
                Thread.sleep(key);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return key;
        }
    }
    

}
