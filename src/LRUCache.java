import java.util.HashMap;
import java.util.Map;

/**
 * An implementation of <tt>Cache</tt> that uses a least-recently-used (LRU)
 * eviction policy.
 */
public class LRUCache<T, U> implements Cache<T, U> {

    /* ------ Implementation of LRU Cache -------- */

    /**
     * The max capacity of the cache. If an element
     * were to be inserted, making the size of the cache greater than
     * this limit, an element would be evicted using LRU policy.
     */
    private final int MAX_CAPACITY;
    /**
     * The number of times an element was not in the cache
     * and had to be fetched from a {@link DataProvider}.
     */
    private int misses = 0;
    /**
     * A {@link HashMap} containing the cache
     * for the given {@link DataProvider}
     */
    private final Map<T, Node<T, U>> cache;
    /**
     * The data provider used to fetch data.
     */
    private final DataProvider<T, U> provider;


    /**
     * @param provider the data provider to consult for a cache miss
     * @param capacity the exact number of (key,value) pairs to store in the cache
     */
    public LRUCache(DataProvider<T, U> provider, int capacity) {
        this.provider = provider;
        this.cache = new HashMap<>(capacity);
        this.MAX_CAPACITY = capacity;
    }

    /**
     * Returns the value associated with the specified key.
     *
     * @param key the key
     * @return the value associated with the key
     */
    public U get(T key) {
        final Node<T, U> attempt = cache.get(key);
        if (attempt == null) {
            //If the key is not in the cache, it's a miss.
            misses++;
            U retrieved = provider.get(key);
            if (this.cache.size() > 0 && this.cache.size() >= MAX_CAPACITY) {
                //Remove the first element from the linked list.
                T removed = removeFirst();
                //remove it from the cache as well
                if (removed != null)
                    this.cache.remove(removed);
            }
            if (this.MAX_CAPACITY > this.cache.size()) {
                Node<T, U> node = addToEnd(key, retrieved);
                //Place the key and value in the cache.
                this.cache.put(key, node);
            }
            return retrieved;
        } else {
            //If the key is in the cache, return the found value.
            moveToEnd(attempt);
            return attempt.value;
        }
    }

    /**
     * Returns the number of cache misses since the object's instantiation.
     *
     * @return the number of cache misses since the object's instantiation.
     */
    public int getNumMisses() {
        return misses;
    }

    /* ------ End Of Implementation of LRU Cache -------- */


    /*------Linked List Implementation--------*/

    //Head and tail of linked list
    private Node<T, U> head = null, tail = null;
    //Number of elements in the list
    private int numElements = 0;

    /**
     * This represents a KeyValue pair stored in a Doubly-LinkedList
     *
     * @param <T> Key
     * @param <U> Value
     */
    private static class Node<T, U> {
        public Node(T key, U value) {
            this.key = key;
            this.value = value;
        }

        private final T key;
        private final U value;
        private Node<T, U> previousPointer;
        private Node<T, U> nextPointer;

        /**
         * Unlinks this node, relinks previous and next
         */
        private void unlink() {
            this.previousPointer.nextPointer = this.nextPointer;
            if (this.nextPointer != null)
                this.nextPointer.previousPointer = this.previousPointer;
        }
    }


    /**
     * Given a node already in the list, move it to the end of the linked list in constant time.
     *
     * @param node the node to be moved.
     */
    private void moveToEnd(Node<T, U> node) {
        if (node.equals(head)) {
            if (tail.equals(node))
                return;
            head = node.nextPointer;
            node.previousPointer = tail;
            node.nextPointer = null;
        } else {
            node.unlink();
            node.previousPointer = tail;
            node.nextPointer = null;
            node.previousPointer.nextPointer = node;
        }
        tail = node;
    }

    /**
     * Add a new node to the end of the linked list.
     *
     * @param key   key of the node
     * @param value value of the node
     * @return the node created and added
     */
    private Node<T, U> addToEnd(T key, U value) {
        Node<T, U> node = new Node<>(key, value);
        if (head == null) {
            head = node;
        } else {
            tail.nextPointer = node;
            node.previousPointer = node;
        }
        tail = node;
        numElements++;
        return node;
    }

    /**
     * Remove the first element of the linked list.
     *
     * @return the removed key
     */
    private T removeFirst() {
        if (numElements == 0) {
            return null;
        } else {
            Node<T, U> prevHead = head;
            head = head.nextPointer;
            if (head != null)
                head.unlink();
            return prevHead.key;
        }
    }

    /*------End Of Linked List Implementation*--------*/


}
